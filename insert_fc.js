(function($) {

  Drupal.behaviors.insert_fc = {};
  Drupal.behaviors.insert_fc.attach = function() {
    var $active_textarea = null;
    var $textareas = $('textarea');
    var $insert_buttons = $('input[rel="insert_fc"]');

    $insert_buttons.each(function() {
      var $button = $(this);

      if($button.hasClass('insert-fc-processed') == false) {
        $button.addClass('insert-fc-processed').click(function(e) {
          e.preventDefault();

          var pos = $button.attr('data-pos');
          var eid = $button.attr('data-eid');
          var field = $button.attr('data-field');
          var view_mode = $('select[rel="insert-fc-view-mode-select-' + pos + '"]').val();
          var entity = {
            type: 'entity',
            field: field,
            view_mode: view_mode
          };

          if(typeof(eid) != 'undefined') {
            entity.eid = eid;
          } else {
            entity.pos = pos;
          }

          console.log(entity);

          insertEntity(entity);
        });
      }
    });

    $textareas.each(function() {
      var $textarea = $(this);

      if($textarea.hasClass('insert-fc-processed') == false) {
        $textarea.addClass('insert-fc-processed').focus(function() {
          $active_textarea = $textarea;
          console.log($textarea);
        });
      }
    });

    function insertEntity(entity) {
      if(typeof(entity.type) == 'undefined') {
        entity.type = 'entity';
      }

      insert('[[' + JSON.stringify(entity) + ']]');
    }

    function insert(content) {
      if($active_textarea === null) {
        return;
      }

      var textarea = $active_textarea.get(0);

      // IE
      if(document.selection) {
        var selection = document.selection.createRange();
        selection-text = content;
      }

      // Mozilla
      else if(textarea.selectionStart || textarea.selectionStart == '0') {
        var start = textarea.selectionStart;
        var end = textarea.selectionEnd;

        textarea.value = textarea.value.substring(0, start) + content + textarea.value.substring(end, textarea.value.length);
      }

      // Fallback
      else {
        textarea.value += content;
      }

      textarea.focus();
    }
  };

})(jQuery);
