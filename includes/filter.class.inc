<?php

class Insert_Fc_Filter {

  protected $node;

  public function __construct(&$node) {
    $this->node = $node;
  }

  public function replace_pos() {
    foreach($this->node as $name => $field) {
      $field_info = field_info_field($name);

      if($field_info['type'] == 'text_long') {
        $values = $field['und'];

        foreach($values as $i => $value) {
          $replaced_value = preg_replace_callback("/\[\[.*?\]\]/s", array($this, 'pos_to_eid'), $value['value']);
          $this->node->{$name}['und'][$i]['value'] = $replaced_value;
        }
      }
    }
  }

  protected function pos_to_eid($match) {
    $match = $match[0];
    $match = str_replace('[[', '', $match);
    $match = str_replace(']]', '', $match);

    $info = drupal_json_decode($match);

    if($info['type'] !== 'entity') {
      return '[[' . $match . ']]';
    }

    if(isset($info['eid'])) {
      return '[[' . $match . ']]';
    }

    $eid = $this->node->{$info['field']}['und'][$info['pos']]['value'];
    $info['eid'] = $eid;

    unset($info['pos']);

    return '[[' . drupal_json_encode($info) . ']]';
  }

}
